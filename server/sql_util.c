#include "sql_util.h"

//连接数据库
MYSQL* mysql_connect(const char* host, const char* user, const char* passwd, char* db) {
    MYSQL* mysql = mysql_init(NULL);

    MYSQL* conn = mysql_real_connect(mysql, host, user, passwd, db, 0, NULL, 0);
    if (!conn) {
        fprintf(stderr, "%s.\n", mysql_error(mysql));
        exit(1);
    } else {
        WriteLog("OUTPUT:MySQL连接成功\n");
    }
    return mysql;
}

// 只查询一个字段
int select_one1(MYSQL* mysql, const char* sql, char* str) {
    if (mysql_query(mysql, sql)) {//成功返回0，失败-1
        fprintf(stderr, "(%d, %s)\n", mysql_errno(mysql), mysql_error(mysql));
        return -1;
    }
    MYSQL_RES* res = mysql_use_result(mysql);
    if (res) {
        int num_fileds = mysql_num_fields(res);// 获取字段数
        MYSQL_ROW row;// 获取每一行信息
        while ((row = mysql_fetch_row(res)) != NULL) {
            // 处理每一行信息
            for (int i = 0; i < num_fileds; i++) {
                //printf("%s\n", row[i]);
                strcpy(str, row[i]);
                //printf("sql, str %s\n", str);
            }
        }
    }
    mysql_free_result(res);
    return 0;
}
// add by sjk 2024.6.23
int select_one(MYSQL* mysql, const char* sql, char* str) {
    if (mysql_query(mysql, sql)) { // 成功返回0，失败-1
        fprintf(stderr, "(%d, %s)\n", mysql_errno(mysql), mysql_error(mysql));
        return -1;
    }

    MYSQL_RES* res = mysql_use_result(mysql);
    if (res) {
        MYSQL_ROW row; // 获取每一行信息
        if ((row = mysql_fetch_row(res)) != NULL) {
            if (row[0] != NULL) {
                strcpy(str, row[0]);
            } else {
                strcpy(str, ""); // 如果是NULL，返回空字符串
            }
        } else {
            strcpy(str, ""); // 如果结果集为空，返回空字符串
        }
        mysql_free_result(res);
        return 0;
    } else {
        if (mysql_field_count(mysql) == 0) {
            // 没有结果集，表示执行的是非查询语句
            fprintf(stderr, "No result set returned\n");
        } else {
            fprintf(stderr, "(%d, %s)\n", mysql_errno(mysql), mysql_error(mysql));
        }
        return -1;
    }
}

// 查询多个字段,多行
int select_multiple(MYSQL* mysql, const char* sql, char* str, int str_len) {
    if (mysql_query(mysql, sql)) {//成功返回0，失败-1
        fprintf(stderr, "(%d, %s)\n", mysql_errno(mysql), mysql_error(mysql));
        return -1;
    }
    MYSQL_RES* res = mysql_use_result(mysql);
    if (res) {
        int num_fileds = mysql_num_fields(res);// 获取字段数
        MYSQL_ROW row;
        while ((row = mysql_fetch_row(res)) != NULL) {// 获取每一行信息
            for (int i = 0; i < num_fileds; i++) {// 处理每一行信息
                                                  //printf("%s\n", row[i]);
                if ((strlen(str) + strlen(row[i]) + 4) > str_len) {
                    printf("Error: Buffer for result string is too small.\n");
                    mysql_free_result(res);// 释放结果集合并返回
                    return 0;
                }
                // 将结果拼接到str中
                strcat(str, row[i]);
                strcat(str, "    ");
               // sprintf(str,"%s    ",row[i]);
                //printf("sql, str %s\n", str);
            }
            strcat(str, "\n");
        }
        
    }
    mysql_free_result(res);
    return 0;
} 

int sql_execute(MYSQL* mysql, const char* sql) {
    // 检查传入的参数是否有效
    if (mysql == NULL || sql == NULL) {
        fprintf(stderr, "Invalid MySQL connection or SQL query.\n");
        return -1;
    }
    while (mysql_next_result(mysql) == 0) {
        MYSQL_RES *result = mysql_store_result(mysql);
        if (result) {
            mysql_free_result(result);
        }
    }
    // 执行SQL语句
    if (mysql_query(mysql, sql)) {
        fprintf(stderr, "SQL execution error: %s\n", mysql_error(mysql));
        return -1;
    }

    // 检查受影响的行数
    int affected_rows = mysql_affected_rows(mysql);
    if (affected_rows == -1) {
        fprintf(stderr, "Error getting affected rows: %s\n", mysql_error(mysql));
        return -1;
    }

    return affected_rows;
}

int sql_execute_old(MYSQL* mysql, const char* sql) {
    if (mysql_query(mysql, sql)) {
        fprintf(stderr, "(%d, %s)\n", mysql_errno(mysql), mysql_error(mysql));
        return -1;
    }

    MYSQL_RES* res = mysql_use_result(mysql);
    if (res) {
        int num_fileds = mysql_num_fields(res);
        MYSQL_ROW row;
        while ((row = mysql_fetch_row(res)) != NULL) {
            for (int i = 0; i < num_fileds; i++) {
                printf("%s\t", row[i]);
            }
            printf("\n");
        }
        mysql_free_result(res);// add by sjk 不加这个有问题
    } else {
        // 执行非查询类操作, update, insert, delete
        if (mysql_field_count(mysql) == 0) {
            // 获取上一个查询操作的结果的字段数
            int num_rows = mysql_affected_rows(mysql);
            return num_rows;
        } else {
            fprintf(stderr, "(%d, %s)\n", mysql_errno(mysql),mysql_error(mysql));
            return -1;
        }
    }
    return 0;
}

// 执行查询并返回结果的函数
QueryResult* execute_query(MYSQL* mysql, const char* sql) {
    if (mysql_query(mysql, sql)) {
        fprintf(stderr, "(%d, %s)\n", 
                mysql_errno(mysql),
                mysql_error(mysql));
        return NULL;
    }

    MYSQL_RES* res = mysql_store_result(mysql);
    if (res == NULL) {
        fprintf(stderr, "(%d, %s)\n", 
                mysql_errno(mysql),
                mysql_error(mysql));
        return NULL;
    }

    int num_rows = mysql_num_rows(res);
    int num_cols = mysql_num_fields(res);

    // 分配结果集结构体内存
    QueryResult* result = (QueryResult*)malloc(sizeof(QueryResult));
    if (result == NULL) {
        fprintf(stderr, "Memory allocation failed\n");
        mysql_free_result(res);
        return NULL;
    }

    // 分配行指针数组内存
    result->rows = (char***)malloc(num_rows * sizeof(char**));
    if (result->rows == NULL) {
        fprintf(stderr, "Memory allocation failed\n");
        mysql_free_result(res);
        free(result);
        return NULL;
    }

    // 分配列数据内存
    for (int i = 0; i < num_rows; i++) {
        result->rows[i] = (char**)malloc(num_cols * sizeof(char*));
        if (result->rows[i] == NULL) {
            fprintf(stderr, "Memory allocation failed\n");
            mysql_free_result(res);
            free(result->rows);
            free(result);
            return NULL;
        }
    }

    // 读取结果集数据并存储到 result->rows
    MYSQL_ROW row;
    int i = 0;
    while ((row = mysql_fetch_row(res))) {
        for (int j = 0; j < num_cols; j++) {
            result->rows[i][j] = strdup(row[j] ? row[j] : "NULL");
            if (result->rows[i][j] == NULL) {
                fprintf(stderr, "Memory allocation failed\n");
                // 释放已分配的内存
                free_query_result(result);
                mysql_free_result(res);
                return NULL;
            }
        }
        i++;
    }

    // 设置结果集行数和列数
    result->num_rows = num_rows;
    result->num_cols = num_cols;

    mysql_free_result(res);
    return result;
}

// 释放查询结果内存的函数
void free_query_result(QueryResult* result) {
    if (result == NULL) {
        return;
    }

    // 释放列数据内存
    for (int i = 0; i < result->num_rows; i++) {
        for (int j = 0; j < result->num_cols; j++) {
            free(result->rows[i][j]);
        }
        free(result->rows[i]);
    }
    // 释放行指针数组内存
    free(result->rows);
    // 释放结果集结构体内存
    free(result);
}





















