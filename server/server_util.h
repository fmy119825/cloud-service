#include <time.h>
#include <stdarg.h>
#define LOG_FILE "server_logfile.log"

// 传递给客户端的信息，用于打印bash前缀
typedef struct {
     char user_name[20];
     char pwd[128];
 }curr_info_t;       
//写日志文件

//读取配置文件信息

//日志函数
/*
    日志函数
    日志的路径写在 LOG_FILE中
    使用方式类似printf
    int a=666;
    log_message("日志用法类似%d", a);
    输出
    [2024-06-17 20:11:13] pid: 3612, tid: 130611593307968, 日志用法类似printf:666
*/
void WriteLog(const char *format, ...);

// 获取key对应的值
int GetIniKeyString(char *section,char *key,char *filename,char *buf);
 
// 修改key对应的值
int PutIniKeyString(char *section,char *key,char *val,char *filename);

//获取文件大小
long get_file_size(const char* filename);

