#include "thread_pool.h"
#include "server_util.h"
//主线程调用:处理客户端发过来的消息
void handleMessage(int sockfd, int epfd, task_queue_t * que)
{
    //消息格式：cmd content
    //1.1 获取消息长度
    int length = -1;
    int ret = recvn(sockfd, &length, sizeof(length));
    WriteLog("handleMessage--recv length: %d", length);

    //1.2 获取消息类型
    int cmdType = -1;
    ret = recvn(sockfd, &cmdType, sizeof(cmdType));
    WriteLog("handleMessage--recv cmd type: %d", cmdType);
    task_t *ptask = (task_t*)calloc(1, sizeof(task_t));
    ptask->peerfd = sockfd;
    ptask->epfd = epfd;
    ptask->type= cmdType;
    if(length > 0) {
        //1.3 获取消息内容
        ret = recvn(sockfd, ptask->data, length);
        if(ret > 0) {
            if(ptask->type == CMD_TYPE_PUTS){
                delEpollReadfd(epfd, sockfd);
            }
            //往线程池中添加任务
            taskEnque(que, ptask);
        }
    } else if(length == 0){
        taskEnque(que, ptask);
    }

    if(ret == 0) {//连接断开的情况
        WriteLog("handleMessage--conn %d is closed.", sockfd);
        delEpollReadfd(epfd, sockfd);
        close(sockfd);
    }
}

//注意：此函数可以根据实际的业务逻辑，进行相应的扩展
void doTask(task_t * task)
{
    assert(task);
    switch(task->type) {
    case CMD_TYPE_PWD:  
        pwdCommand(task);   break;
    case CMD_TYPE_CD:
        cdCommand(task);    break;
    case CMD_TYPE_LS:
        lsCommand(task);    break;
    case CMD_TYPE_MKDIR:
        mkdirCommand(task);  break;
    case CMD_TYPE_RMDIR:
        rmdirCommand(task);  break;
    case CMD_TYPE_NOTCMD:
        notCommand(task);   break;
    case CMD_TYPE_PUTS:
        putsCommand(task);
        addEpollReadfd(task->epfd, task->peerfd);
        break;
    case CMD_TYPE_GETS:
        getsCommand(task);   break;
    // wmx-6.19
    case LOGIN_STEP_ONE:
        userLoginCheckOne(task); break;
    case LOGIN_STEP_TWO:
        userLoginCheckTwo(task); break;
    case REGISTER_STEP_ONE:
        userRegisterOne(task); break;
    case REGISTER_STEP_TWO:
        userRegisterTwo(task); break;
    }

}
