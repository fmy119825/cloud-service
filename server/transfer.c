#include "thread_pool.h"
#define FILENAME "bigfile.ev4"


int transferFile(int sockfd)
{
    //进行文件的发送
    //1. 先发送文件名
    //1.1 设置文件名的长度
    train_t t;
    memset(&t, 0, sizeof(t));
    t.len = strlen(FILENAME);
    strcpy(t.buff, FILENAME);
    send(sockfd, &t, 4 + t.len, 0);

    //2. 读取文件内容( 相对路径 )
    int fd = open(FILENAME, O_RDWR);
    ERROR_CHECK(fd, -1, "open");
    memset(&t, 0, sizeof(t));

    //2.1 获取文件的长度
    struct stat fileInfo;
    memset(&fileInfo, 0, sizeof(fileInfo));
    fstat(fd, &fileInfo);
    off_t length = fileInfo.st_size;
    printf("file length: %ld\n", length);

    //发送文件的长度
    sendn(sockfd, &length, sizeof(length));

    //借助于一条管道来搬运数据
    int fds[2];
    int ret = -1;
    int curSize = 0;
    pipe(fds);

    //发送文件内容
    while(curSize < length) {
        ret = splice(fd, NULL, fds[1], NULL, 4096, SPLICE_F_MORE);
        ret = splice(fds[0], NULL, sockfd, NULL, ret, SPLICE_F_MORE);
        curSize += ret;
    }
    printf("curSize:%d\n", curSize);
    close(fd);
    return 0;
}
//1，2期
//6.16 Fmy 发送数据使用 其他的命令也可以用
void transferTaskData(task_t *task) {
    int peerfd = task->peerfd;

    // 发送数据长度
    int data_len = strlen(task->data);
    if (send(peerfd, &data_len, sizeof(data_len), 0) < 0) {
        perror("Send data length failed");
        close(peerfd);
        exit(1);
    }

    // 发送数据内容
    if (send(peerfd, task->data, data_len, 0) < 0) {
        perror("Send data failed");
        close(peerfd);
        exit(1);
    }
    printf("task->data:%s\n",task->data);
    printf("Task data sent to client successfully.\n");

    // 在这里可以根据需要继续发送其他数据或处理其他任务
}
//6.16 Fmy end



