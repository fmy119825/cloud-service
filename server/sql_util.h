#ifndef __HANDLESQL_H__
#define __HANDLESQL_H__

#include <mysql/mysql.h>
#include "thread_pool.h"
#include "server_util.h"


MYSQL* mysql_connect(const char* host, const char* user, const char* passwd, char* db);
int select_one(MYSQL* mysql, const char* sql, char* str);
int select_multiple(MYSQL* mysql, const char* sql, char* str, int str_len);
int sql_execute(MYSQL* mysql, const char* sql);
// 函数声明：执行查询并返回结果
QueryResult* execute_query(MYSQL* mysql, const char* sql);

// 函数声明：释放查询结果内存
void free_query_result(QueryResult* result);


#endif
