#pragma once

#include <func.h>

typedef enum {
    CMD_TYPE_PWD = 1,
    CMD_TYPE_LS,
    CMD_TYPE_CD,
    CMD_TYPE_MKDIR,
    CMD_TYPE_RMDIR,
    CMD_TYPE_PUTS,
    CMD_TYPE_GETS,
    CMD_TYPE_NOTCMD,  //不是命令
                      // 用于登录和注册
    LOGIN_STEP_ONE = 666,
    LOGIN_STEP_ONE_OK,
    LOGIN_STEP_ONE_ERR,
    LOGIN_STEP_TWO,
    LOGIN_STEP_TWO_OK,
    LOGIN_STEP_TWO_ERR,

    REGISTER_STEP_ONE,
    REGISTER_STEP_ONE_OK,
    REGISTER_STEP_ONE_ERR,
    REGISTER_STEP_TWO,
    REGISTER_STEP_TWO_OK,
    REGISTER_STEP_TWO_ERR

}CmdType;

typedef struct 
{
    int len;  //记录内容长度
    CmdType type;  //消息类型
    char buff[1000];  //记录内容本身
}train_t;

int tcpConnect(const char * ip, unsigned short port);
int recvn(int sockfd, void * buff, int len);
int sendn(int sockfd, const void * buff, int len);

int parseCommand(const char * input, int len, train_t * pt);
void login(int clientfd);
//判断一个字符串是什么命令
int getCommandType(const char * str);

int getFile(int clientfd);
int putFile(int sockfd, char* filename);

// wmx-6.19
void userLogin(int clientfd);
void userLoginOne(int clientfd, train_t *train);
void userLoginTwo(int clientfd, train_t *train);

void userRegister(int clientfd);
void userRegisterOne(int clientfd, train_t *train);
void userRegisterTwo(int clientfd, train_t *train);

// 数据库结果集结构体定义
typedef struct {
    char ***rows;   // 二维字符数组，存储每行每列的数据
    int num_rows;   // 结果集行数
    int num_cols;   // 结果集列数
} QueryResult;