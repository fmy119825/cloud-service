#include <func.h>
#include <time.h>
#include <stdarg.h>
#include <openssl/evp.h>
#include <openssl/md5.h>

#define LOG_FILE "client_logfile.log"
#define MAXLINE 1024

typedef struct {                                                   
    char user_name[20];
    char pwd[128];
}curr_info_t;

//读取配置文件信息

//写配置文件信息

//日志函数


//分割字符串
void splitString(const char * pstrs, char *tokens[], int max_tokens, int * pcount);
void freeStrs(char * pstrs[], int count);
//
//获取指令类型
int getCommandType(const char* str);
//解析指令
int parseCommand(const char* pinput, int len, train_t* pt);

int recvn(int sockfd, void* buff, int len);
int sendn(int sockfd, const void* buff, int len);

void get_file_md5(const char* filename, unsigned char* md5);

// 读取ini文件中字符类型的值
void read_ini(const char* filename, const char* section, const char* key, char* value, size_t value_size);

// 获取key对应的值
int GetIniKeyString(char *section,char *key,char *filename,char *buf);

// 修改key对应的值
int PutIniKeyString(char *section,char *key,char *val,char *filename);

//日志函数
/*
   日志函数
   日志的路径写在 LOG_FILE中
   使用方式类似printf
   int a=666;
   log_message("日志用法类似%d", a);
   输出
   [2024-06-17 20:11:13] pid: 3612, tid: 130611593307968, 日志用法类似printf:666
   */
void WriteLog(const char *format, ...);
void currinfo_printf(curr_info_t curr);
