#include "client.h"
#include <sys/select.h>
#define CONFIG_NAME "../conf/client.conf"

#include "client_util.h"
#include "transfer.h"

int main()
{
/*
// 获取key对应的值
int GetIniKeyString(char *section,char *key,char *filename,char *buf);
 
// 修改key对应的值
int PutIniKeyString(char *section,char *key,char *val,char *filename);
*/
   
    char str_ip[256];
    char str_port[256];
    char filename[256]=CONFIG_NAME;
   
    int ret=GetIniKeyString("client","ip",filename,str_ip);
    
    ret= GetIniKeyString("client","port",filename,str_port);
    WriteLog("get ret:%d, str_port: %s", ret, str_port);
    int port=atoi(str_port);

    int sockfd = tcpConnect(str_ip, port);
    if(sockfd == -1){
        exit(-1);
    }

    // ***********************************************
    // w--用户进行登录操作
    userLogin(sockfd);
    // ************************************************


    char buf[1024] = { 0 };

    //4. 使用select进行监听
    fd_set rdset;
    train_t t;
    while (1) {
        FD_ZERO(&rdset);
        FD_SET(STDIN_FILENO, &rdset);
        FD_SET(sockfd, &rdset);
        
        int nready = select(sockfd + 1, &rdset, NULL, NULL, NULL);
        // printf("nready: %d\n", nready);
        switch (nready){
        case -1:
            close(sockfd);
            error(1, errno, "select");
        default:
            if (FD_ISSET(STDIN_FILENO, &rdset)) {
                //读取标准输入中的数据
                memset(buf, 0, sizeof(buf));
                int ret = read(STDIN_FILENO, buf, sizeof(buf));
                if (ret == 0) {
                    WriteLog("byebye.");
                    break;
                }
                memset(&t, 0, sizeof(t));
                //解析命令行
                buf[strlen(buf)-1] = '\0';
                parseCommand(buf, strlen(buf), &t);
                int cmdType = getCommandType(buf);
                sendn(sockfd, &t, 4 + 4 + t.len);
                switch (cmdType){
                    case CMD_TYPE_PWD:
                        //WriteLog("start pwd");
                        recvData(sockfd);
                        recvCurr(sockfd);
                        break;
                    case CMD_TYPE_GETS:
                        //WriteLog("start gets\n");
                        getFile(sockfd);
                        break;
                    case CMD_TYPE_PUTS:
                        //WriteLog("start puts\n");
                        putFile(sockfd, t.buff);
                        break;
                    case CMD_TYPE_LS:
                        WriteLog("start ls");
                        recvData(sockfd);
                        recvCurr(sockfd);
                        //todo  by sjk
                        break;
                    case CMD_TYPE_MKDIR:
                        recvData(sockfd);
                        recvCurr(sockfd);
                        break;
                    case CMD_TYPE_RMDIR:
                        recvData(sockfd);
                        recvCurr(sockfd);
                        break;
                    case CMD_TYPE_CD:
                        WriteLog("start cd");
                        recvData(sockfd);
                        recvCurr(sockfd);
                        break;
                    default:CMD_TYPE_NOTCMD;
                        recvData(sockfd);
                        recvCurr(sockfd);
                        break;
                }
            } else if(FD_ISSET(sockfd, &rdset)) {
                break;
                //recvFile(sockfd);
                //WriteLog(" RECV END");
            }
        }
    }

    close(sockfd);
    return 0;
}
